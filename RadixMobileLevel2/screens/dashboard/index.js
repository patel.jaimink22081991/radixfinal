import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  StatusBar,
  FlatList,
} from "react-native";
import axios from "axios";
import { baseUrl, API_KEY } from "../api";

const DashboardScreen = ({ route, navigation }) => {
  const { user } = route.params;
  const [sourceList, setSourceList] = useState([]);

  const fetchListSources = async () => {
    const url = `${baseUrl}/v2/top-headlines/sources?apiKey=${API_KEY}`;
    await axios
      .get(url)
      .then((response) => {
        if (response.data.status === "ok") {
          const addSelectionVal=[];
          for(var i in response.data.sources){
             var newArr={...response.data.sources[i]}
             newArr.selection = '0'
             addSelectionVal.push(newArr);
          }
          setSourceList(addSelectionVal);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchListSources();
  }, []);

  const renderItem = ({ item }) => (
    <View style={styles.item}>
      <Text style={styles.title}>Name: {item.name}</Text>
      <Text style={styles.title}>category: {item.category}</Text>
      <Text style={styles.title}>country: {item.country}</Text>
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={sourceList}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 16,
  },
});

export default DashboardScreen;
