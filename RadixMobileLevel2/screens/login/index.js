import React from "react";
import { StyleSheet, View, Button } from "react-native";
import * as Google from "expo-google-app-auth";
import { iosClientId } from "../api";

const LoginScreen = ({ navigation }) => {
  const signInAsync = async () => {
    try {
      const { type, user } = await Google.logInAsync({
        iosClientId: iosClientId,
      });

      if (type === "success") {
        navigation.navigate("DashboardScreen", { user });
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View>
      <Button title="Google sign in" onPress={signInAsync} />
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({});
